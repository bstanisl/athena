index_ref
(.*)_mems(.*)
(.*)_timings(.*)

# Ignore most b-tagging outside the recommended taggers: DL1dv01 and GN2v01
xAOD::BTagging(?!.*PFlow.*DL1dv01_p.*)
xAOD::BTagging(?!.*PFlow.*GN2v01_p.*)

# Also ignore some b-tagging on tracking
xAOD::TrackParticleAuxContainer.*\.btagIp_.*

# Ignore Fold variables for b-tagging on PFlow
xAOD::JetAuxContainer.*PFlow.*jetFold.*
